// refreshar spelet varje sekund
function  refreshGamesEachSecond(){
  setInterval(refreshGames, 3000);
}

// anropar en server och få alla spel
function refreshGames() {
  rpsApi.allGames()  // tar emot function i object (object.function)
    .then(games => {
      if (games.length === null || games.length === 'nul') {
        document.getElementById('game-list').innerHTML = "Inga spel tillgängliga";
      }
      let gamesListHtml = games.map(game => {
        return createGameRow(game.gameId, game.name, game.game);
      })
        .join('');
      let gameStatus = games.map(game => {
        return createGameStatus(game.gameId, game.game,);
      })
      document.getElementById('game-list').innerHTML = gamesListHtml;
    });
}

function createGameStatus(gameId, game) {
  return game;
}

/* Hur alla spel ska visas på html sidan */
function createGameRow(gameId, name, game) {
  if (name === null || name === '') {
    name = 'Anonymous';
  }
  if(name === sessionStorage.getItem('userName')){
    return '0'
  }
  return '<li class="text white d-flex flex-row justify-content-around align-items-center pb-1">\n' +
    '    <div class="text-black"> Name: ' + name + ' <br> gameId: '  + gameId + '<br> gameStatus: ' + game + '</div>\n' +
    '<button class="btn btn-primary text-white" formtarget="_blank" onclick="joinGame(\''+gameId+'\')">Anslut</button>\n' +
    '</li>\n' +
    '<hr/>'
}

/* ändras färgen på input vid focus */
function changeColor(x) {
  x.style.background = 'yellow';
}

//sparar namnet som följare och sedan följa ett spel med sparade namn*/
function joinGame(gameId){
  // let jGame = document.getElementById('joinG').value;
  //  sessionStorage.setItem('jGame', jGame)
  rpsApi.setName(sessionStorage.getItem('jGame'));
  console.log("Join game ",gameId);
  rpsApi.joinGame(gameId)
    .then(gameStatus => {
      console.log("Joined game ",gameStatus);
      window.location.href = "../html/player_player.html";

    })
}

/* spara namnet bara i sessionStorage */
function creatN() {
  let jGame = document.getElementById('joinG').value;
  sessionStorage.setItem('jGame', jGame)
  document.getElementById('btn1').style.display = 'none'
  document.getElementById('joinG').style.display = 'none'
  if(jGame === null || jGame === '') {
    document.getElementById('text1').style.display = 'block'
  }
  else {
    document.getElementById('text').style.display = 'block'
  }
}

/* Startar ny spel med skapade namn */
function startRemoteGame() {
  let userName = document.getElementById('nyGame').value;
  sessionStorage.setItem('userName', userName);
  rpsApi.setName(sessionStorage.getItem('userName'))
    .then(ignore => {
      rpsApi.newGame()
        .then(gameStatus => {
          window.location.href = 'player_player.html';
        });
    })
}

refreshGamesEachSecond();
