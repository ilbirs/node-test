const rpsApi = {

  //sparar en token
  setToken: (token) => {
    sessionStorage.setItem('token', token);
  },

  //tar emot token
  getToken: () => {
    return sessionStorage.getItem('token');
  },

  // skapar ny token
  newToken: () => {
    fetch('https://java19.sensera.se/auth/token')
      .then(response => response.text())
      .then(text => sessionStorage.setItem('token', text))
  },

  // tar emot all spel som finns i server
  allGames: () => {
    return fetch('https://java19.sensera.se/games', {headers: {'token': sessionStorage.getItem('token')}})
      .then(response => response.json())
  },

  //Starta en ny game
  newGame: () => {
    return fetch('https://java19.sensera.se/games/start', {headers: {'token': sessionStorage.getItem('token')}})
      .then(response => response.json())
  },

  //anropar man spel och få svaret tillbaka genom json
  joinGame: (gameid) => {
    return fetch('https://java19.sensera.se/games/join/' + gameid, {headers:{'token': sessionStorage.getItem('token')}})
      .then(response => response.json())
  },

  // hämta spel status hela scheman med gameId, name, opponeName, move, oppoMov, game
  // name: den som startat ett spel
  //opponentname: den som joinat ett spel
  getGameStatus: () => {
    return fetch('https://java19.sensera.se/games/status', {headers: {'token': sessionStorage.getItem('token')}})
      .then(response => response.json())
  },


  makeMove: (sign) => {
    return fetch('https://java19.sensera.se/games/move/'+sign, {headers: {'token': sessionStorage.getItem('token')}})
      .then(response => response.json())
  },


  //skapar ny name och sparar den
  setName: (name) => {
    return fetch('https://java19.sensera.se/user/name', {
      method: 'POST',
      body: JSON.stringify({"name": name}),
      headers: {'token': sessionStorage.getItem('token'), 'Content-Type': 'application/json;charset=UTF-8'}
    })
      .then(response => response.text())
  }
};

if (sessionStorage.getItem('token') == null) { //om token finns ska ny token inte hämtas.
  rpsApi.newToken();// om man ta bor det så komer vi aldrig få ny token
}
