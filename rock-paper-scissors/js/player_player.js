let user1Score = 0;
let user2Score = 0;
let badge1;
let badge2;
let p1_label;
let p2_label;
let res_label;
const result_Tat = document.getElementById('res-move')
let user1Label = document.getElementById("user1-label");
let user2Label = document.getElementById("user2-label");
let ny_Game = document.getElementById('nyGame');
let play_Btn = document.getElementById('btn');
const result_p = document.querySelector(".result > p");
const rock_div = document.getElementById("rock");
const paper_div = document.getElementById("paper");
const scissors_div = document.getElementById("scissors");
const player1Move = document.getElementById("player1-move");
const player2Move = document.getElementById("player2-move");

/* tar sparat name och sätter den till webläsare
let userName = sessionStorage.getItem('userName')
if (userName) {
    document.getElementById('user1-label').innerHTML = userName;
    console.log('userName', userName);
}
if (userName == null || userName === '') {
    userName = 'Anonymous';
}*/



/* Nollställar spelet*/
function nollFunction() {
  p1_label = "";
  p2_label = "";
  res_label = "";
  player1Move.value = p1_label;
  player2Move.value = p2_label;
  result_Tat.value = res_label;
  document.querySelector(".result > p").style.display = 'block';
}

/* Functioner vid vinst, förlust, samt oavgjord som visas på display*/
function win(user1Choice, user2Choice) {
  player1Move.value = user1Choice;
  player2Move.value = user2Choice;
  result_Tat.value = 'WIN';
  document.getElementById('res-move').style.background = 'green';
  result_p.innerHTML = " Du vann den här omgången! "
}

function lose(user1Choice, user2Choice) {
  player1Move.value = user1Choice;
  player2Move.value = user2Choice;
  result_Tat.value = 'LOSE';
  document.getElementById('res-move').style.background = 'red';
  result_p.innerHTML = " Du förlurar den här omgången! "
}

function draw(user1Choice, user2Choice) {
  player1Move.value = user1Choice;
  player2Move.value = user2Choice;
  result_Tat.value = 'DRAW';
  document.getElementById('res-move').style.background = 'blue';
  result_p.innerHTML = " det är oavgjord! "
}

//server ska anropa val genom knapen
function gameMove(sign) {
  rpsApi.makeMove(sign) //anropar function
    .then(gameStatus => {

    });
}
/* Gör att knapar fungerarp */
function main() {
  rock_div.addEventListener('click', function () {
    gameMove("ROCK");
  })
  paper_div.addEventListener('click', function () {
    gameMove("PAPER");
  })
  scissors_div.addEventListener('click', function () {
    gameMove("SCISSORS");
  })

}

main();

/*refreshar game status värje sekund*/
function refreshGameStatusEachSecond() {
  myVar = setInterval(refreshGameStatus, 3000);
}

// hen som joinar hamnar här
function refreshGameStatus() {
  console.log("refreshGameStatus");
  //anropar gameStatus
  rpsApi.getGameStatus()
    //den som komma tilbaka - gameStatus
    .then(gameStatus => {
      console.log("refreshGameStatus.getGameStatus", gameStatus);
      document.getElementById("user1-label").innerHTML = gameStatus.name;

      /* visar på vilken sidan man ska stå som joinad s game */
      if (gameStatus.opponentName) {
        document.getElementById('user2-label').innerHTML = gameStatus.opponentName;
      }
      /* visas vad ska stå på label om man inte skapad eller joinad a game*/
      if (gameStatus.opponentName === null || gameStatus.opponentName === '') {
        gameStatus.opponentName = "J_Anonymous";
        document.getElementById('user2-label').innerHTML = 'Anonymous'
      }
      if(gameStatus.name === null || gameStatus.name === '') {
        gameStatus.name = 'S_Anonymous';
        document.getElementById('user1-label').innerHTML = 'Ananymous'
      }
      switch (gameStatus.game) {
        case 'WIN':
          win(gameStatus.move, gameStatus.opponentMove);
          break;
        case 'LOSE':
          lose(gameStatus.move, gameStatus.opponentMove);
          break;
        case 'DRAW':
          draw(gameStatus.move, gameStatus.opponentMove);
          break;
      }
      /* Spel avslutning*/
      if (gameStatus.game === 'WIN' || gameStatus.game === 'LOSE' || gameStatus.game === 'DRAW' ) {
        clearInterval(myVar)
        console.log('Spelet avslutat');
      }
    });

}

refreshGameStatusEachSecond();





