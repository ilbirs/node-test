let userScore = 0;
let compScore = 0;
let badge;
let p_label;
let comp;
const userScore_span = document.getElementById("user-score");
const compScore_span = document.getElementById("comp-score");
const scoreBoard_div = document.querySelector(".score-board");
const result_p = document.querySelector(".result > p");
const rock_div = document.getElementById("rock");
const paper_div = document.getElementById("paper");
const scissors_div = document.getElementById("scissors");
const userForm_form = document.getElementById("user-form");
const useName = document.getElementById("use-name");
const userLabel = document.getElementById("user1-label");
const playerMove = document.getElementById("player-move");
const compMove = document.getElementById("comp-move");

/*öppnar modalen2 när man viner eller förlurar*/
function endGame1(){
  document.querySelector(".result > p").innerHTML = "Du vann den här omgången!";
  document.getElementById('id02').style.display='block';

}
function endGame2(){
  document.querySelector(".result > p").innerHTML = "Du förlurar den här omgången";
  document.getElementById('id03').style.display='block';
}

/* Nollställar spelet*/
function nollFunction(){
  userScore = 0;
  compScore = 0;
  badge = 'player';
  p_label = "";
  comp = "";
  userScore_span.innerHTML = userScore;
  compScore_span.innerHTML = compScore;
  userLabel.innerHTML = badge;
  playerMove.value = p_label;
  compMove.value = comp;
  document.getElementById('id02').style.display = 'none';
  document.getElementById('id03').style.display='none';
}

/* Functioner som stänger modal2*/
function closeModal2(){
  userScore = 0;
  compScore = 0;
  p_label = "";
  comp = "";
  userScore_span.innerHTML = userScore;
  compScore_span.innerHTML = compScore;
  playerMove.value = p_label;
  compMove.value = comp;
  document.getElementById('id02').style.display = 'none';
}
function closeModal3(){
  userScore = 0;
  compScore = 0;
  p_label = "";
  comp = "";
  userScore_span.innerHTML = userScore;
  compScore_span.innerHTML = compScore;
  playerMove.value = p_label;
  compMove.value = comp;
  document.getElementById('id03').style.display='none';
}

/* Öppnar modalen1, stänger den efter registreringen
 samt stänger 'Close' and 'Avbryt' knappar */
function modalFunction() {
  document.getElementById('id01').style.display = 'block';
}
function closeFunction(){
  document.getElementById('id01').style.display = 'none';
}
function regFunction() {
  if (useName.value)
  {
    document.getElementById('id01').style.display = 'none';
  }
}
function cancelFunction() {
  document.getElementById('id01').style.display = 'none';
}

/*  Hela modalen1 STÄNGS om man klickar utanför modalen*/
const modal = document.getElementById('id01');
window.onclick = function(event) {
  if (event.target === modal) {
    modal.style.display = "none";
  }
}

/*registrera namnet som spelare*/
userForm_form.onsubmit = (event) => {
  event.preventDefault();
  let item = useName.value;
  userLabel.innerHTML = `<div>${item}</div>`;
  useName.value = '';
}
/* computer väljer en av de tre val */
function getCompChoice(){
  const choices = ['rock','paper','scissors'];
  const randomNumber = Math.floor(Math.random() *3);
  return choices[randomNumber];
}

/* Functioner vid vinst, förlust, samt oavgjord*/
function win (userChoice, compChoice) {
  userScore ++;
  userScore_span.innerHTML = userScore;
  compScore_span.innerHTML = compScore;
  playerMove.value = userChoice;
  compMove.value = compChoice;
  result_p.innerHTML = userChoice + " beats " + compChoice + " . You win! "
}
function lose (userChoice, compChoice) {
  compScore++
  userScore_span.innerHTML = userScore;
  compScore_span.innerHTML = compScore;
  playerMove.value = userChoice;
  compMove.value = compChoice;
  result_p.innerHTML = userChoice + " lose to " + compChoice + " . You lose! "
}
function draw (userChoice, compChoice) {
  playerMove.value = userChoice;
  compMove.value = compChoice;
  result_p.innerHTML = userChoice + " equals " + compChoice + ".  It's draws. ! "
}

/*jämföra värdet*/
function game(userChoice) {
  const compChoice = getCompChoice();
  switch(userChoice + compChoice) {
    case "rockscissors":
    case "paperrock":
    case "scissorspaper":
      win(userChoice, compChoice);
      break;
    case "rockpaper":
    case "paperscissors":
    case "scissorsrock":
      lose(userChoice, compChoice);
      break;
    case "rockrock":
    case "paperpaper":
    case "scissorsscissors":
      draw(userChoice, compChoice);
      break;
  }
  /*Spelet spelas upp till 5 poäng*/
  if(userScore === 5) {
    endGame1()
  }
  if(compScore === 5){
    endGame2()
  }
}
function main() {
  rock_div.addEventListener('click', function () {
    game("rock");
  })
  paper_div.addEventListener('click', function () {
    game("paper");
  })
  scissors_div.addEventListener('click', function () {
    game("scissors");
  })
}
main();
